# Grading Poker

Cards to help with grading.

Including / unifying grading scales:

- Study Regulations (Prüfungsordnung)
	- in the department Art & Design
	- at the UE University of Applied Sciences Europe

![GradingOverview](gradingOverview_240310.png)

## Why...?

- better (shared) understanding of grading scale
	- what is a 1.3? what is a 2.7? what are 83%?
- enforces
	- clear and early communication (and with this a shared understanding) of
		- the assignment / task itself (content, size, etc.)
		- the grading criteria
- better understanding ones own grade

## How To...?

?

## Log / Version-History

### Todos / Bugs / Ideas

- improve printing abilities
	- adjust size
	- get lost of the outlines
	- (maybe) adjust for card printing service ([meinspiel.de](https://www.meinspiel.de/blog/spielkarten-fuer-digitaldruck/), etc.)

### Release 0.3 (10.3.2024)

- removed Progression Quest & Grading Sheet points
- adjusted card-designs (& added one more extimation card) to communicate better the difference between 4.0 and 5.0 "fail"

### Release 0.2 (27.9.2021)

- added estimation cards

### Release 0.1 (7.10.2019)

- first version
- ok visualization
- emphasizing the contradictions between some included grading scales

## Credits

**csongor baranyai**

- csongorb (at) gmail (dot) com  
- [www.csongorb.com](http://www.csongorb.com)

Made with:

- [OmniGraffle Pro](https://en.wikipedia.org/wiki/OmniGraffle), Version 7.21.5

## License

[![Creative Commons License](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc/4.0/)
This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/).

All other media (images, software, etc.) remain the property of their copyright holders.
