# Grading Poker - Process Journal

## Process Journal

### 26th of September 2021

The yearly update. Small adjustments in the estimation cards to avoid misunderstandings (that the highest and the lowest cards are not immediately meant as THE highest and lowest estimation, but as a tendency).

### 8th of September 2020

The cards are (for certain things, not everything) too detailed. For a certain type of discussion, at a certain step of the (grading) process a more rough segmentation is needed. Curious to try out if this works.

### 6th of October 2019

How to unify all the different grading principles? Especially that they are contradicting each other.

- leaving the contradictions, visualizing them clearly?
- trying to unify and simplify?

### 4th of October 2019

I'm trying to gamify / playify my teaching since forever.

- [Progression Quest](https://bitbucket.org/csongorb/progressionquest)
- [PornCreator](https://bitbucket.org/csongorb/porncreator/)
	- [Dissecting Games](http://dissectinggames.csongorb.com)
	- [Narrative Design Porn](http://narrativedesignporn.csongorb.com)
	- [Level Design Porn](http://leveldesignporn.csongorb.com)
- [The Fun of It - A personal heap of fragments and quotes about teaching and game design](https://medium.com/@csongorb/the-fun-of-it-b80852a19834)

And miserably failing, mainly by not consequently enough realizing simple gamification / game design insights. Grading is not really in the magic circle. Externally visible (and important) valuation is clashing with intrinsic motivations. There is no such thing as playful grading.

The idea is to emphasize the advantages / benefits of some game design knowledge (am I allowed to say this?) and ideas (clear communication, progress, shaping the environment for learning, etc.) without denying that this is not a game.

This is not a game.

## Related Work

The original [Planning Poker paper by James Grenning](https://wingman-sw.com/articles/planning-poker), incl. some notes and further reading on why you should try avoid it (and use Planning Poker Party instead).
